# Makefile for CloudFormation
STACKNAME="change-set-test"
TEMPLATE="file://./basic.json"
#CSN="change-01"

stack:
	aws cloudformation create-stack --stack-name $(STACKNAME) --template-body $(TEMPLATE) 

change-set:
	aws cloudformation create-change-set --stack-name $(STACKNAME) --template-body $(TEMPLATE)  --change-set-name $(CSN) 

update:	
	aws cloudformation update-stack --stack-name $(STACKNAME) --template-body $(TEMPLATE) 

clean:
	aws cloudformation delete-stack --stack-name $(STACKNAME) 